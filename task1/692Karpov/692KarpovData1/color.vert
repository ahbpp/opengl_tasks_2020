#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec3 color;

void main()
{
    float z = vertexPosition.z;

    // (255, 255*3/4, 0) -  на максимумах высоты
    // (0, 255/4, 255) -  на минимумах высоты
    color = vec3(z, z / 4.f + (1-z) * 2.5f / 4.f, (1 - z) / 1.2f);
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}