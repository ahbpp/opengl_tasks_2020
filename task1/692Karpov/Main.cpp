#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>

class SampleApplication : public Application
{
public:
    MeshPtr terrain;

    ShaderProgramPtr shader;


    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей

        terrain = makeTerrain();
        terrain->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-60.0f, -60.0f, 2.0f)));

        _cameraMover = std::make_shared<FreeCameraMover>();


        //=========================================================
        //Инициализация шейдеров

        shader = std::make_shared<ShaderProgram>("692KarpovData2/color.vert", "692KarpovData2/color.frag");

    }


    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        shader->setMat4Uniform("modelMatrix", terrain->modelMatrix());

        terrain->draw();

    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}