#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>


#include <iostream>

class SampleApplication : public Application
{
public:
    MeshPtr terrain;
    ShaderProgramPtr shader;

    MeshPtr _marker;
    ShaderProgramPtr _markerShader;



    float _lr = 15.0f;
    float _phi = 15.0f;
    float _theta = 15.0f;

    LightInfo _light;

    TexturePtr _grassTex;
    TexturePtr _sandTex;
    TexturePtr _lavaTex;
    TexturePtr _snowTex;
    TexturePtr _maskTex;

    GLuint _samplerGrass;
    GLuint _samplerSand;
    GLuint _samplerLava;
    GLuint _samplerSnow;
    GLuint _samplerMask;


    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей

        terrain = makeTerrain(0.002f, 0.4f);
        terrain->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-120.0f, -120.0f, 0.0f)));

        _marker = makeSphere(0.5f);


        _cameraMover = std::make_shared<FreeCameraMover>();


        //=========================================================
        //Инициализация шейдеров

        shader = std::make_shared<ShaderProgram>("692KarpovData2/shader.vert", "692KarpovData2/shader.frag");
        _markerShader = std::make_shared<ShaderProgram>("692KarpovData2/marker.vert", "692KarpovData2/marker.frag");


        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        _grassTex = loadTexture("692KarpovData2/grass.jpg");
        _sandTex = loadTexture("692KarpovData2/sand.jpg");
        _lavaTex = loadTexture("692KarpovData2/lava.jpg");
        _snowTex = loadTexture("692KarpovData2/snow.jpg");
        _maskTex = loadTexture("692KarpovData2/mask.png");

        glGenSamplers(1, &_samplerGrass);
        glSamplerParameteri(_samplerGrass, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerGrass, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerGrass, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerGrass, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_samplerSand);
        glSamplerParameteri(_samplerSand, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerSand, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerSand, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerSand, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_samplerLava);
        glSamplerParameteri(_samplerLava, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerLava, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerLava, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerLava, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_samplerSnow);
        glSamplerParameteri(_samplerSnow, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerSnow, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerSnow, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerSnow, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_samplerMask);
        glSamplerParameteri(_samplerMask, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerMask, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerMask, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerMask, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 40.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 4.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f,4.0f * glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        shader->use();

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        shader->setVec3Uniform("light.La", _light.ambient);
        shader->setVec3Uniform("light.Ld", _light.diffuse);
        shader->setVec3Uniform("light.Ls", _light.specular);

        glBindSampler(0, _samplerGrass);
        glActiveTexture(GL_TEXTURE0);
        _grassTex->bind();
        shader->setIntUniform("grassTex", 0);

        glBindSampler(1, _samplerLava);
        glActiveTexture(GL_TEXTURE1);
        _lavaTex->bind();
        shader->setIntUniform("lavaTex", 1);

        glBindSampler(2, _samplerSnow);
        glActiveTexture(GL_TEXTURE2);
        _snowTex->bind();
        shader->setIntUniform("snowTex", 2);

        glBindSampler(3, _samplerMask);
        glActiveTexture(GL_TEXTURE3);
        _maskTex->bind();
        shader->setIntUniform("maskTex", 3);

        glBindSampler(4, _samplerSand);
        glActiveTexture(GL_TEXTURE4);
        _sandTex->bind();
        shader->setIntUniform("sandTex", 4);


        {
            shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
            shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
            shader->setMat4Uniform("modelMatrix", terrain->modelMatrix());
            shader->setMat3Uniform("normalToCameraMatrix",
                                    glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * terrain->modelMatrix()))));

            terrain->draw();
        }

        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);


    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}