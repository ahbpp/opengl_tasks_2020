#include <Texture.hpp>

#include <SOIL2.h>

#include <vector>
#include <iostream>


TexturePtr loadTexture(const std::string& filename, SRGB srgb, bool prefer1D)
{
    int width, height, channels;
    unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height, &channels, SOIL_LOAD_AUTO);
    if (!image)
    {
        std::cerr << "SOIL loading error: " << SOIL_last_result() << std::endl;
        return std::make_shared<Texture>();
    }


    GLint internalFormat;
    if (srgb == SRGB::YES)
    {
        internalFormat = (channels == 4) ? GL_SRGB8 : GL_SRGB8_ALPHA8;
    }
    else
    {
        internalFormat = (channels == 4) ? GL_RGBA8 : GL_RGB8;
    }

    GLint format = (channels == 4) ? GL_RGBA : GL_RGB;

	bool is1DTexture = (height == 1);
	GLenum target = (prefer1D && is1DTexture) ? GL_TEXTURE_1D : GL_TEXTURE_2D;

    TexturePtr texture = std::make_shared<Texture>(target);
	if (is1DTexture) {
		texture->setTexImage1D(target, 0, internalFormat, width, format, GL_UNSIGNED_BYTE, image);
	}
	else {
		texture->setTexImage2D(target, 0, internalFormat, width, height, format, GL_UNSIGNED_BYTE, image);
	}
    texture->generateMipmaps();

    SOIL_free_image_data(image);

    return texture;
}
